package com.example.antonija.v18_z1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonija on 19/09/2017.
 */

public class TaskDBHelper extends SQLiteOpenHelper {

    private static TaskDBHelper _TaskDBHelperInstance = null;

    private TaskDBHelper(Context context) {
        super(context, Schema.DATABASE_NAME, null, Schema.DATABASE_VERSION);
    }

    public static synchronized TaskDBHelper getInstance(Context context) {
        if (_TaskDBHelperInstance == null) {
            context = context.getApplicationContext();
            _TaskDBHelperInstance = new TaskDBHelper(context);
        }
        return _TaskDBHelperInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SqlQueries.CREATE_TABLE_TASKS);
    //    db.execSQL(SqlQueries.CREATE_TABLE_CATEGORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SqlQueries.DROP_TABLE_TASKS);
    //    db.execSQL(SqlQueries.DROP_TABLE_CATEGORY);
        this.onCreate(db);
    }

    public void insertTask(Task task) {
        ContentValues values = new ContentValues();
        values.put(Schema.TASKS_TITLE, task.get_Title());
        values.put(Schema.TASKS_PRIORITY, task.get_Priority());
     //   values.put(Schema.TASKS_CATEGORY, task.get_Category());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(Schema.TABLE_NAME, null, values);
        String[] conditionzero = new String[]{" "};
        database.delete(Schema.TABLE_NAME, Schema.TASKS_TITLE + "=?", conditionzero);
        Log.d("Task", "New task added");
    }

    public List<Task> getTasks() {
        SQLiteDatabase database = this.getWritableDatabase();
        String[] conditionzero = new String[]{" "};
        database.delete(Schema.TABLE_NAME, Schema.TASKS_TITLE + "=?", conditionzero);
        String[] columns = new String[]{Schema.TASKS_TITLE, Schema.TASKS_PRIORITY};
        Cursor result = database.query(Schema.TABLE_NAME, columns, null, null, null, null, null);
        return parseTasks(result);
    }

    private List<Task> parseTasks(Cursor result) {
        List<Task> tasks = new ArrayList<>();
        if (result.moveToFirst()) {
            do {
                String title = result.getString(result.getColumnIndex(Schema.TASKS_TITLE));
                String priority = result.getString(result.getColumnIndex(Schema.TASKS_PRIORITY));
             //   String category = result.getString(result.getColumnIndex(Schema.TASKS_CATEGORY));

                Task task = new Task(title, priority);
                tasks.add(task);
            }
            while (result.moveToNext());
            result.close();
        }
        return tasks;
    }


    public void deleteTaskAt(Task task) {
        SQLiteDatabase database = this.getWritableDatabase();
        String[] condition = new String[]{task.get_Title()};
        database.delete(Schema.TABLE_NAME, Schema.TASKS_TITLE + "=?", condition);
        database.close();
    }

  /*  public void insertCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(Schema.CATEGORY_NAME, category.get_category());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(Schema.TABLE_CATEGORY, null, values);
        database.close();
    }

    public List<Category> getCategories()
    {
        SQLiteDatabase database = this.getWritableDatabase();
        String[] column = new String[]{Schema.CATEGORY_NAME;
        Cursor resultc = database.query(Schema.TABLE_CATEGORY, column, null, null, null, null, null);
        return parseCategory(resultc);
    }
        private List<Category> parseCategory (Cursor resultc){
            List<Category> categories = new ArrayList<>();
            if (resultc.moveToFirst()) {
                do {
                    String name = resultc.getString(resultc.getColumnIndex(Schema.CATEGORY_NAME));

                    Category category = new Category(name);
                    categories.add(category);
                }
                while (resultc.moveToNext());
                resultc.close();
            }
            return categories;
        }
    }
    */
}
