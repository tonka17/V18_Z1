package com.example.antonija.v18_z1;

/**
 * Created by Antonija on 19/09/2017.
 */

public class SqlQueries {
    public static final String CREATE_TABLE_TASKS = "CREATE TABLE "
            + Schema.TABLE_NAME + " ("
            + Schema.TASKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Schema.TASKS_TITLE + " TEXT NOT NULL, "
            + Schema.TASKS_PRIORITY + " TEXT NOT NULL, "
            + Schema.TASKS_CATEGORY + " TEXT NOT NULL" + ")";
    public static final java.lang.String DROP_TABLE_TASKS = "DROP TABLE IF EXISTS " + Schema.TABLE_NAME;

  /*  public static final String CREATE_TABLE_CATEGORY = "CREATE TABLE "
            + Schema.TABLE_CATEGORY + " ("
            + Schema.CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Schema.CATEGORY_NAME + " TEXT NOT NULL" + ")";
    public static final java.lang.String DROP_TABLE_CATEGORY = "DROP TABLE IF EXISTS " + Schema.TABLE_CATEGORY;
 */
}
