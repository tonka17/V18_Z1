package com.example.antonija.v18_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;
import butterknife.OnItemSelected;

public class MainActivity extends Activity {

    private static final int KEY_REQUEST_TITLE = 10;
    public static final String KEY_TASK_TITLE = "task_title";
    public static final String KEY_TASK_PRIORITY = "task_priority";
  //  public static final String KEY_TASK_CATEGORY = "task_category";
    @BindView(R.id.lvTaskList) ListView lvTaskList;
    @BindView(R.id.bGoToList)Button bGoToList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        TaskAdapter taskadapter = new TaskAdapter(this);
        lvTaskList.setAdapter(taskadapter);


    }

    @OnClick(R.id.bGoToList)
    public void goToList()
    {
        Intent intent = new Intent(this.getApplicationContext(),TasklistActivity.class);
        startActivityForResult(intent, KEY_REQUEST_TITLE);
    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        String task_title = " ";
        String task_priority = " ";
    //    String task_category = " ";
        if(requestCode==KEY_REQUEST_TITLE)
        {
            if(resultCode==RESULT_OK)
            {
                if(data!=null && data.hasExtra(KEY_TASK_TITLE))
                {
                    task_title = data.getStringExtra(KEY_TASK_TITLE);

                }
            }
        }

        if(data!=null && data.hasExtra(KEY_TASK_PRIORITY))
        {
            task_priority = data.getStringExtra(KEY_TASK_PRIORITY);

        }
        /*     if(data!=null && data.hasExtra(KEY_TASK_CATEGORY))
        {
            task_category = data.getStringExtra(KEY_TASK_CATEGORY);

        }*/
        Task task = new Task(task_title, task_priority);

        TaskAdapter taskadapter = (TaskAdapter) lvTaskList.getAdapter();
        taskadapter.addTask(task);

    }
    @OnItemLongClick(R.id.lvTaskList)
    public boolean deleteTask(int position)
    {

        TaskAdapter taskadapter = (TaskAdapter) this.lvTaskList.getAdapter();
        taskadapter.deleteTaskAt(position);
        return true;
    }
}

