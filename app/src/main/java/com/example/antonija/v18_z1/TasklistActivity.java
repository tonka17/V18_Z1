package com.example.antonija.v18_z1;

import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.Spinner;

        import com.squareup.picasso.Picasso;

        import butterknife.BindView;
        import butterknife.ButterKnife;
        import butterknife.OnClick;
        import butterknife.OnItemClick;
        import butterknife.OnItemSelected;

public class TasklistActivity extends Activity {

    @BindView(R.id.etTitle)
    EditText etTitle;
    @BindView(R.id.sPriority)
    Spinner sPriority;
    @BindView(R.id.bAddTask)
    Button bAddTask;
 /*   @BindView(R.id.bAddCategory)
    Button bAddCategory;
    @BindView(R.id.sCategory)
    Spinner sCategory;

*/
    ArrayAdapter<CharSequence> spinnerAdapter;
    String SPriority;
  //  ArrayAdapter<CharSequence> spinnerAdapterCat;
  //  String SCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasklist);
        ButterKnife.bind(this);

        spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.PriorityArray, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sPriority.setAdapter(spinnerAdapter);

    }

    @OnItemSelected(R.id.sPriority)
    public void SelectPriority(int position) {
        SPriority = (String) sPriority.getItemAtPosition(position);
    }

  /*  @OnClick(R.id.bAddCategory)
    public void goToAddCategory()
    {
        Intent intent = new Intent();
        intent.setClass(this.getApplicationContext(), CategoryActivity.class);
        this.startActivity(intent);
    }
*/
    @OnClick(R.id.bAddTask)
    public void addTask()
    {
        Intent intent = new Intent();
        String title = etTitle.getText().toString();
        String priority = " ";
   //     String category = " ";
        if(SPriority.equals("Low")) {
            priority = "Low";
        }
        else if(SPriority.equals("Medium")) {
            priority = "Medium";
        }
        else if(SPriority.equals("High")) {
            priority = "High";
        }
        intent.putExtra(MainActivity.KEY_TASK_TITLE, title);
        intent.putExtra(MainActivity.KEY_TASK_PRIORITY, priority);

     //   intent.putExtra(MainActivity.KEY_TASK_CATEGORY, category);
        setResult(RESULT_OK, intent);
        this.finish();
    }

}