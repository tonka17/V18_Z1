package com.example.antonija.v18_z1;

/**
 * Created by Antonija on 21/09/2017.
 */

public class Category {
    public String _category;

    public Category() {
    }

    public Category(String _category) {
        this._category = _category;
    }

    public String get_category() {
        return _category;
    }
}
