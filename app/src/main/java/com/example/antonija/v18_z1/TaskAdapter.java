package com.example.antonija.v18_z1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 19/09/2017.
 */

public class TaskAdapter extends BaseAdapter {
    TaskDBHelper _TaskDBHelper;
    List<Task> _TaskList;

    public TaskAdapter(Context context)
    {
        this._TaskDBHelper = TaskDBHelper.getInstance(context);
        this._TaskList = _TaskDBHelper.getTasks();
    }


    @Override
    public int getCount() {
        return this._TaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return this._TaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

     TaskHolder holder;
     if(convertView==null)
     {
         convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_task, parent, false);
         holder = new TaskHolder(convertView);
         convertView.setTag(holder);
     }
     else
     {
         holder = (TaskHolder) convertView.getTag();
     }

        Task task = this._TaskList.get(position);
        holder.tvTitle.setText(task.get_Title());
        holder.tvPriority.setText(task.get_Priority());
     //   holder.tvCategory.setText(task.get_Category());

     return convertView;

    }

    public void addTask(Task task) {
        this._TaskDBHelper.insertTask(task);
        this._TaskList = this._TaskDBHelper.getTasks();
        this.notifyDataSetChanged();
    }

    public void deleteTaskAt(int position) {
        Task task = this._TaskList.get(position);
        this._TaskDBHelper.deleteTaskAt(task);
        this._TaskList = this._TaskDBHelper.getTasks();
        this.notifyDataSetChanged();
    }


    static class TaskHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvPriority) TextView tvPriority;
    //    @BindView(R.id.tvCategory) TextView tvCategory;
        //    @BindView(R.id.iPriority) ImageView iPriority;

        public TaskHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
