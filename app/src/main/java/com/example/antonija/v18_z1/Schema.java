package com.example.antonija.v18_z1;

/**
 * Created by Antonija on 19/09/2017.
 */

public class Schema {

    //database info
    public static final String DATABASE_NAME = "database";
    public static final int DATABASE_VERSION = 1;

    //table info:
    public static final String TABLE_NAME = "tasks";
  //  public static final String TABLE_CATEGORY = "categories";
    //columns:
    public static final String TASKS_ID = "_id";
    public static final String TASKS_TITLE = "title";
    public static final String TASKS_PRIORITY = "priority";
    public static final String TASKS_CATEGORY = "category";
  //  public static final String CATEGORY_ID = "_id";
  //  public static final String CATEGORY_NAME = "category name";
}
