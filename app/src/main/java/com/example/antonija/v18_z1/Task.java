package com.example.antonija.v18_z1;

/**
 * Created by Antonija on 19/09/2017.
 */

public class Task {
    String _Title;
    String _Priority;
  //  String _Category;

    public Task(String _Title, String _Priority) {
        this._Title = _Title;
        this._Priority = _Priority;
    //    this._Category = _Category;
    }

    public String get_Title() {
        return _Title;
    }

    public String get_Priority() {
        return _Priority;
    }

 //   public String get_Category() { return _Category; }
}

