package com.example.antonija.v18_z1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends Activity {

    @BindView(R.id.etCategory)
    EditText etCategory;
    @BindView(R.id.bAddCategory)
    Button bAddCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
    }

 /*   @OnClick(R.id.bAddCategory)
    public void addCategory()
    {
        String newCategory = etCategory.getText().toString();
        Category category = new Category(newCategory);
        this._TaskDBHelper.insertCategory(category);
    }*/
}
